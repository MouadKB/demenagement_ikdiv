$(document).ready(function(){/*start of document ready*/
$(".plus").click(function(){
	var name=$(this).parent().attr("data-name");/*select data-name of current article seulement pour l'affichage*/
	var aid=$(this).parent().attr("id");/*select id of current article*/
	var volume=parseFloat($(this).parent().attr("data-volume"));/*select volume of current article pour le calcul*/
	var volumetotal=parseFloat($("#second").attr("data-volume-total"));/*select volume-total pour le calcul*/
	var c=parseInt($(this).parent().attr("data-c"));/*select compteur of current article pour le calcul*/
	c++;/*incrementation du compteur de l'article */
	volumetotal+=volume;/*update of volumetotal*/
	$(this).parent().attr("data-c", c);/*update of data-c*/
	$("#second").attr("data-volume-total", volumetotal);/*update of data-volume-total*/
	$("#second").html(volumetotal.toFixed(2)+' m<sup>3</sup>');/*affichage du nouveau volumetotal*/
	var aids=aid;
	var aid='r'+aids;
	var cid='c'+aids;
	if(c==1)
	{
	$('#result-table tr:last').after('<tr id="'+cid+'"><td>' + name + '</td><td id="'+aid+'">' + c + '</td></tr>');
	}else{
	$('#' + aid).text(c);	
	}
});
$(".minus").click(function(){
	var c=parseInt($(this).parent().attr("data-c"));/*select compteur of current article*/
	var aid=$(this).parent().attr("id");/**/
	var aids=aid;
	var aid='r'+aids;
	var cid='c'+aids;
	var volume=parseFloat($(this).parent().attr("data-volume"));/*select volume of current article*/
	var volumetotal=parseFloat($("#second").attr("data-volume-total"));/*select  volume-total*/
	if(c>1){
	c--;/*incrementation du  compteur*/
	volumetotal-=volume;
	$(this).parent().attr("data-c", c);/*update of data-c*/
	$("#second").attr("data-volume-total", volumetotal);/*update of data-volume-total*/
	$("#second").html(volumetotal.toFixed(2)+' m<sup>3</sup>');/*affichage du nouveau volumetotal*/
	$('#' + aid).text(c);	
	}else if(c==1) {
	c=0;
	volumetotal-=volume;
	$("#second").attr("data-volume-total", volumetotal);
	$(this).parent().attr("data-c", c);/*update of data-c*/
	$("#second").html(volumetotal.toFixed(2)+' m<sup>3</sup>');/*affichage du nouveau volumetotal*/
	$('#'+cid).remove();	
	}else{
	$('#'+cid).remove();		
	}
	$(document).ready(function() { 
		$("button").click(function() { 
			//$("#d").trigger("reset"); 
			//$("#d").get(0).reset(); 
			$("#d")[0].reset() 
		}); 
	}); 
});
});/*end of document ready*/